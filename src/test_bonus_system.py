from bonus_system import calculateBonuses

STANDARD = "Standard"
PREMIUM = "Premium"
DIAMOND = "Diamond"

def test_invalid_cases():
    assert calculateBonuses("", 5000) == 0
    assert calculateBonuses("", 10000) == 0
    assert calculateBonuses("", 20000) == 0
    assert calculateBonuses("", 50000) == 0
    assert calculateBonuses("", 70000) == 0
    assert calculateBonuses("", 100000) == 0
    assert calculateBonuses("", 200000) == 0
    assert calculateBonuses("Sta", 5000) == 0
    assert calculateBonuses("Pre", 5000) == 0
    assert calculateBonuses("Dia", 5000) == 0

def test_standard_program():
    assert calculateBonuses(STANDARD, 5000) == 0.5
    assert calculateBonuses(STANDARD, 10000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 20000) == 0.5 * 1.5
    assert calculateBonuses(STANDARD, 50000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 70000) == 0.5 * 2
    assert calculateBonuses(STANDARD, 100000) == 0.5 * 2.5
    assert calculateBonuses(STANDARD, 200000) == 0.5 * 2.5

def test_premium_program():
    assert calculateBonuses(PREMIUM, 5000) == 0.1
    assert calculateBonuses(PREMIUM, 10000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 20000) == 0.1 * 1.5
    assert calculateBonuses(PREMIUM, 50000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 70000) == 0.1 * 2
    assert calculateBonuses(PREMIUM, 100000) == 0.1 * 2.5
    assert calculateBonuses(PREMIUM, 200000) == 0.1 * 2.5

def test_diamond_program():
    assert calculateBonuses(DIAMOND, 5000) == 0.2
    assert calculateBonuses(DIAMOND, 10000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 20000) == 0.2 * 1.5
    assert calculateBonuses(DIAMOND, 50000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 70000) == 0.2 * 2
    assert calculateBonuses(DIAMOND, 100000) == 0.2 * 2.5
    assert calculateBonuses(DIAMOND, 200000) == 0.2 * 2.5
